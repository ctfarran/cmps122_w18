#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <openssl/conf.h>
#include <openssl/evp.h>
#include <openssl/err.h>

#include "crypto.h"

int main(int argc, char *argv[])
{
	char *CIPHER_FILE = argv[1];
	unsigned char *KEY = (unsigned char *) argv[2];
	unsigned char *IV = (unsigned char *) argv[3];
	char *PLAIN_FILE = argv[4];

	FILE *crypt_file = fopen(CIPHER_FILE, "rb");
	fseek(crypt_file, 0, SEEK_END);
	int filelen = ftell(crypt_file);
	rewind(crypt_file);

	FILE *plain_file = fopen(PLAIN_FILE, "wb"); 

	unsigned char decrypt_text[3000];
	memset(decrypt_text, '\0', 3000);
	
	unsigned char cipher_text[3000];
	memset(cipher_text, '\0', 3000);

	fread(cipher_text, filelen, 1, crypt_file);

	int dlen = decrypt(cipher_text, filelen-1, KEY, IV, decrypt_text);

	decrypt_text[dlen] = '\0';

	fwrite(decrypt_text, sizeof(char), dlen, plain_file);

	fclose(crypt_file);	
	fclose(plain_file);
}
