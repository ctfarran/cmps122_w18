#https://stackoverflow.com/questions/8886947/caesar-cipher-function-in-python

with open("sonnet66","r") as fl:
	plainText = fl.read()
#plainText = raw_input("What is your plaintext? ")
shift = 6

def caesar(plainText, shift): 
    cipherText = ""
    for ch in plainText:
	shift += 1
	if (shift == 10):
	    shift = 6
        if ch.isalpha():
            stayInAlphabet = ord(ch) + shift 
            if stayInAlphabet > ord('z'):
                stayInAlphabet -= 26
            finalLetter = chr(stayInAlphabet)
	    #print (cipherText)
            cipherText += finalLetter

    print "Your ciphertext is: ", cipherText

    return cipherText

caesar(plainText, shift)