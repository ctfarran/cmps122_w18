#!/usr/bin/env python
import sys

def polybius_decrypt(cipherStr, dictionary):
# infile = "message43.cipher.output438"
    print cipherStr
    tokens = list(cipherStr)
    #print tokens
    tokens_enum = iter(range(len(tokens)))
    tokens2 = []
    for i in tokens_enum:
        if tokens[i].isdigit() and tokens[i+1].isdigit():
            tokens2.append(tokens[i] +tokens[i+1])
            tokens_enum.next()
        else:
            tokens2.append(tokens[i])
    #print(tokens2)
    poly_square = \
    [['a','b','c','d','e'], \
    ['f','g','h','i','j'], \
    ['k','l','m','n','o'], \
    ['p','q','r','s','t'], \
    ['u','v','w','x','y']]

    #print(poly_square)

    for i, token in enumerate(tokens2):
        if(len(token) == 2):
            tokens2[i] = poly_square[int(token[0]) -1] [int(token[1]) -1]
    message_plain = ''.join(tokens2)
    return message_plain


#print( polybius_decrypt("message43.cipher.output438", "message43.plain.output438"))
