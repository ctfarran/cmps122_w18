//caleb farrand ctfarran
// Avery Iosa
// Julius Mazer

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "crypto.h"

// https://stackoverflow.com/questions/3501338/c-read-file-line-by-line
 char *getFileContents(char* fileName,int fileLen){
	FILE* fp = fopen(fileName,"rb");
	if(fp == NULL){
		return "";
	}
	char *contents = malloc(sizeof(char)*fileLen+1);
	contents[fileLen] = '\0';
	fread(contents,fileLen,1,fp);
	//printf("contents: %s",contents);
    return contents;
}

/*int decrypt(unsigned char *ciphertext, int ciphertext_len, unsigned char *key,
  unsigned char *iv, unsigned char *plaintext);
 */

int main(int argc, char *argv[]) {
	int cipherLen = atoi(argv[2]);
	char* cipherText = getFileContents(argv[1],cipherLen);
	char plainText[cipherLen+1];
	int byteRead = decrypt(cipherText,cipherLen,argv[3],argv[4],plainText);
	plainText[byteRead] = '\0';
	printf("%s\n",plainText);
}