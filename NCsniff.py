import socket
import sys
import time

#from scapy.all import *

TCP_IP = "128.114.59.42"
port = 5001


s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((TCP_IP, port))
v = 0
while True:
	result = s.recv(3000)
	if (result == "EMPTY"):
		continue
	else:
		v += 1
		filename = "f" + str(v) + ".pcap"
		with open(filename, "wb") as myfile:
			    myfile.write(result)

		#scapy.wrpcap(filename,result)
		print(result)
		print(" ----------------------------")