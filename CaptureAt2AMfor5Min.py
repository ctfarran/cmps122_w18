import socket
import sys
from datetime import datetime
from threading import Timer
import time



#from scapy.all import *
x=datetime.today()
y=x.replace(day=x.day, hour=17, minute=2, second=0, microsecond=0)
delta_t=y-x

secs=delta_t.seconds+1

TCP_IP = "128.114.59.42"
port = 5001

def start_capture():
	print("Start Capturing")
	start = time.time()
        PERIOD_OF_TIME = 500 # 5min
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	s.connect((TCP_IP, port))
	v = 0
	while True:
		result = s.recv(3000)
		#if (result == "EMPTY"):
		#	continue
		#else:
		v += 1
		filename = "CapturePhaseThree" + str(v) + ".pcap"
		with open(filename, "wb") as myfile:
			myfile.write(result)

		#scapy.wrpcap(filename,result)
		print(result)
		print(" ----------------------------")
		if (time.time() > start + PERIOD_OF_TIME):
                        print("overtime")
                        break

t = Timer(secs, start_capture)
t.start()