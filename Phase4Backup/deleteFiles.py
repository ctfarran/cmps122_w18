#!/usr/bin/env python

import os, glob
import random

dir_files = glob.glob("dir*")

for mydir in dir_files:
	os.chdir(mydir)
	# pcap_files = glob.glob("message*")
	# pcap_files = sorted(pcap_files, key=lambda x: int(x[6:]))	
	# rand_int = random.randint(3, 4)
	str_1 = "shopt -s extglob"
	str_2 = "rm !(mypcap*)"

	os.system(str_1)
	os.system(str_2)
	os.chdir("..")
