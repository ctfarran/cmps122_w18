#!/usr/bin/env python

import os, glob
import shutil

cwd = os.getcwd()
files = glob.glob("4*")
files = sorted(files, key=lambda x: int(x))


dir_count = 0
x = 0
while x  len(files)-1:
	pcap_dir = cwd + "/dir" + str(dir_count)
	if not os.path.exists(pcap_dir):
		os.makedirs(pcap_dir)
		shutil.move(files[x], pcap_dir)
		shutil.move(files[x+1], pcap_dir)
		shutil.move(files[x+2], pcap_dir)
		shutil.move(files[x+3], pcap_dir)
		shutil.move(files[x+4], pcap_dir)
		shutil.move(files[x+5], pcap_dir)
	dir_count+= 1
	x += 6
