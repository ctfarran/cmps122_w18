#!/usr/bin/env python

import cracker
import glob, os

dir_list = glob.glob("dir*")

for mydir in dir_list:
	print(mydir)



dir_list = sorted(dir_list, key=lambda x: int(x[3:]))
encrypted_pass = []

for mydir in dir_list:
	files_in_dir = glob.glob(mydir + "/4*")
	files_in_dir = sorted(files_in_dir, key=lambda x: int(x[6:]))
	#print(files_in_dir[0])
	myfile = open(files_in_dir[0], "r")
	passwd_cipher = myfile.readline().rstrip()
	print(passwd_cipher)
	print(files_in_dir[0])
	passwd_plain = cracker.crack(passwd_cipher)
	passwd_file = open(mydir + "/passwd.plain", "w+")
	print(passwd_plain + files_in_dir[0])
	passwd_file.write(passwd_plain)
	myfile.close()
	passwd_file.close()

