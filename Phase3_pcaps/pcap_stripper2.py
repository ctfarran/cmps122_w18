#!/usr/bin/env python

import sys
import glob
from scapy.all import *
from scapy.utils import *

def getLoad(fileName):
	packets = rdpcap(fileName)
	packet = packets[0]
	return packet.load

def writeFile(load, fileName):
	f = open(fileName, "wb")
	f.write(load)
	f.close

pcap_list = glob("*.pcap")
#fileName = sys.argv[1]
for pcap in pcap_list:
	load = getLoad(pcap)
	writeFile(load, pcap[:-5])

