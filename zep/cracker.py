import crypt, getpass, pwd

Chars = ['0s','1B','2w','3B','4h','5a','6t','7L','8p','9k','At','BB','Ch','Da','Ea','FC','Gs','Hf','Im','Ja','Km','Ls','Ma','Ni','Om','Pb','Qh','RW','St','Tf','Ui','Va','Wt','Xc','Yw','Zb','a0','b3','c6','d9','ec','fy','gc','hC','ip','je','kd','lt','mi','nw','oS','pb','qT','rh','sC','tI','us','va','wA','xC','yH', 'zo']
salts = ['pa','ss','ep','ar','tp','ut','it']
def crack(encryptedPassword):
	#ThePasswords = open("ThePassords", "w")
	for w1 in Chars:
		for w2 in Chars:
			for w3 in Chars:
				guess = w1+w2+w3
				salt = encryptedPassword[0:2]
				x = crypt.crypt(guess, salt)
				if (x == encryptedPassword):
					print ("pass:", guess)
					print ("hash:", encryptedPassword)
					return guess
	return "kek"