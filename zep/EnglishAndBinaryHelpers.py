# from nltk.corpus import words
# """
# https://stackoverflow.com/questions/3788870/how-to-check-if-a-word-is-an-english-word-with-python
# """
# def checkEnglishRatio(message):
# 	total = 0.0
# 	englishWordCount = 0.0
# 	for word in message.split():
# 		word = word.rstrip().rstrip(",.?):;\t '").lstrip("('\t ").lower()
# 		if word in words.words():
# 			englishWordCount +=1.0
# 		total += 1.0
# 	print "wordcount is ",englishWordCount
# 	print "total is ",total
# 	return float(englishWordCount / total)

# def checkOccasionalEnglishRatio(message):
# 	total = 0.0
# 	englishWordCount = 0.0
# 	counter = 0
# 	for word in message.split():
# 		if counter % 5:	
# 			word = word.rstrip().rstrip(",.?):;\t '").lstrip("('\t ").lower()
# 			if word in words.words():
# 				englishWordCount +=1.0
# 			total += 1.0
# 		counter += 1
# 	print "wordcount is ",englishWordCount
# 	print "total is ",total
# 	return float(englishWordCount / total)

# https://stackoverflow.com/questions/44296167/how-to-check-if-a-string-contains-binary-data
def isBinary(message): # PEP 8 naming - snake_words not mixedCase
	return any(ord(x) > 0x80 for x in message)

def checkAsciiRatio(message):
	engCount = 0.0
	total = 0.0
	for word in message:
		if ord(word) <= 0x80:
			engCount += 1.0
		total += 1.0
	ratio = engCount / total
	print "engCount: " + str(engCount) + " total: " + str(total)
	print "ratio is ",ratio
	return ratio