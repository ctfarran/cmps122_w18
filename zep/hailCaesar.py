import re
import EnglishAndBinaryHelpers

def readFromFile(fileName):
	f = open(fileName,"r")
	output = ""
	for line in f:
		output += line
	f.close()
	return output
def writeToFile(fileName,content):
	f = open(fileName,"w")
	f.write(content)
	f.close()
def checkMessage(message,dictionary):
	for word in dictionary.split():
		if word.strip() in message:
			print "word ",word.strip() 
			return True
	return False
def decodeCaesar(cipher,index):
	asciiNumRange = range(48,58)
	asciiUpperRange = range(65,91)
	asciiLowerRange = range(97,123)
	message = ""
	for letter in cipher:
		o = ord(letter)
		if (o not in asciiUpperRange and o not in asciiLowerRange) or o in asciiNumRange:
			yield o
		else: 
			newO = o + index % 26
			if o in asciiLowerRange and newO in asciiLowerRange:
				yield newO 
			elif o in asciiUpperRange and newO in asciiUpperRange:
				yield newO 
			else:
				yield newO - 26

#https://stackoverflow.com/questions/8886947/caesar-cipher-function-in-python
def hailCaesar(cipher,dictionary):
	for index in range(0,26):
		message =  (''.join(map(chr,decodeCaesar(cipher,index))))
		#print message
		if checkMessage(message,dictionary): #and EnglishAndBinaryHelpers.checkEnglishRatio(message) > .5:
			print "index is ",index
			print "message is ",message
			return message
	return "Not found"
			#break
#cipher = readFromFile("../Phase3/mag1")
#hailCaesar(cipher)
