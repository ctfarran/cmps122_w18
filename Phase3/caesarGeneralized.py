import sys

# usege
# python caesarGeneralized.py [filename] [left limit (ex:6)] [right limit (ex:10)] [start number (ex:7)]
shift_left_limit = int(sys.argv[2])
shift_right_limit = int(sys.argv[3])
shift_start = int(sys.argv[4])
filename = sys.argv[1]
print (shift_left_limit,shift_right_limit, shift_start)

decrypt_list = []
def caesar(encrypt_list, left, right, start):
        for c in encrypt_list:
                if start == right:
                        start = left

                if c == ' ':
                        decrypt_list.append(c)
                        start = start + 1
                        continue

                # convert c to an int
                c_num = ord(c) - start

                if c_num < 65:
                        c_num = c_num + 26
                elif c_num >= 88 and c_num < 97:
                        c_num = c_num + 26

                decrypt_list.append(chr(c_num))
                start = start + 1

encrypt_list = []
with open(filename) as f:
        while True:
                c = f.read(1)
                if not c:
                        break

                alpha_num = c.isalpha()
                if alpha_num or c == ' ':
                        encrypt_list.append(c)
                else:
                        continue

print (encrypt_list)
caesar(encrypt_list, shift_left_limit, shift_right_limit, shift_start)

decrypt_fd = open("decrypt.txt", "w+")
for c in decrypt_list:
        decrypt_fd.write(c)

decrypt_fd.close()
