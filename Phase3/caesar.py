#!/usr/bin/env python

decrypt_list = []
def caesar(encrypt_list, shift):
	for c in encrypt_list:
		if shift == 10:
			shift = 6

		if c == ' ':
			decrypt_list.append(c)
			shift = shift + 1
			continue

		# convert c to an int
		c_num = ord(c) - shift
			
		if c_num < 65:
			c_num = c_num + 26
		elif c_num >= 88 and c_num < 97:
			c_num = c_num + 26

		decrypt_list.append(chr(c_num))	
		shift = shift + 1	

encrypt_list = []
with open("sonnet_encrypt.txt") as f:
	while True:
		c = f.read(1)
		if not c:
			break

		alpha_num = c.isalpha()
		if alpha_num or c == ' ':
			encrypt_list.append(c)
		else:
			continue
		
caesar(encrypt_list, 7)

decrypt_fd = open("decrypt.txt", "w+")
for c in decrypt_list:
	decrypt_fd.write(c)

decrypt_fd.close()
