#!/usr/bin/env python

# Write all pcap files to allpcaps directory

import os, socket, datetime, time

IP = '128.114.59.42'
PORT = 5001

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((IP, PORT))
cwd = os.getcwd()

s.settimeout(300)
counter = 0

now = datetime.datetime.now()
hours = now.hour
minutes = now.minute

# while True:
# 	now = datetime.datetime.now()
# 	hours = now.hour
# 	minutes = now.minute

# 	if (hours == 1 and minutes >= 56):
# 		break

while True:
	# if time >= 2:30:00 or counter >= 3000		
	# if (hours == 2 and minutes >= 10)  or counter >= 300:
	# 	print("Breaking from loop")
	# 	break
	toDelete = False
	file_path = cwd + "/pcapfiles/char_pcap/" + "mypcap" + str(counter) + ".pcap"
	pcapfile = open(file_path, "wb")

	try:
		pcapbuffer = s.recv(65536)
		counter = counter + 1
	except socket.timeout:
		print("Socket timeout")
		break

	for item in pcapbuffer:
		if item is not None:
			pcapfile.write(item)
        else:
            toDelete = True

	pcapfile.close()
	if toDelete == True:
        	os.remove(file_path)
        	toDelete = False

s.close()

