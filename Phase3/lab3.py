# Caleb Farrand ctfarran
# Avery Iosa
# Julius Mazer jmazer

import os,re,subprocess, socket
from threading import Thread
# from nltk.corpus import wordnet
from scapy.all import *
from scapy.utils import *
#Takes in a .zip.pcap file and returns a .zip file.
# Courtesy of https://stackoverflow.com/questions/34293209/pythonscapy-raw-load-found-how-to-access
def readPCAP(fileName):
	pack = rdpcap(fileName)
	print pack
	packet = pack[0]
	return packet.getlayer(Raw).load
def writeZipFile(zipFileName,content):
	zipFile = open(zipFileName,"w")
	zipFile.write(content)
	zipFile.close()
	#TODO write code to verify zip file was written correctly

def readFromFile(fileName):
	f = open(fileName,"r")
	output = ""
	for line in f:
		output += line
	f.close()
	return output
def writeToFile(fileName,content):
	f = open(fileName,"w")
	f.write(content)
	f.close()
def readBinaryFromFile(fileName):
	f = open(fileName,"rb")
	output = ""
	for line in f:
		output += line
	return output
def decryptMessage(decryptPath, cipherFile, keyText, ivFile):
	cipherText = readBinaryFromFile(cipherFile).rstrip('\n')
	#keyText = readFromFile(keyFile).rstrip('\n')
	iv = readFromFile(ivFile).rstrip('\n')
	cipherLen = len(cipherText)
	#print "cypherLen ",cipherLen
	command = decryptPath + "/FindKey " + cipherFile + " " + str(cipherLen) + " " + keyText + " " + iv
	#print "command is ", command
	output = subprocess.check_output(command,shell=True)
	return output

"""
https://stackoverflow.com/questions/3788870/how-to-check-if-a-word-is-an-english-word-with-python
"""
# def checkEnglish(message):
# 	messageList = message.split()
# 	total = 0
# 	englishWordCount = 0
# 	for word in messageList:
# 		if wordnet.synsets(word):
# 			englishWordCount +=1
# 		total += 1
# 	return englishWordCount / total

# https://stackoverflow.com/questions/44296167/how-to-check-if-a-string-contains-binary-data
def isBinary(str): # PEP 8 naming - snake_words not mixedCase
	return any(ord(x) > 0x80 for x in str)

def findKeyAndMessage(decryptPath, cipherFile, keyFile, ivFile):
	keyText = readFromFile(keyFile).rstrip()
	limit = len(keyText)-32
	print "iv: \n",readFromFile(ivFile)
	print "key: \n\n",readFromFile(keyFile)
	for i in range(limit):
		message = ""
		try:
			message = decryptMessage(decryptPath,cipherFile,keyText[i:i+32],ivFile)
			print "possible correct message\n",message
		except:
			continue
		if message != "":
			if not isBinary(message) or "Sonnet" in message:
				print "returning message"
				return (keyText[i:i+32], message)
				#print "correct message\n",message
				#print "Correct key is ",keyText
	print "did not find the thing."
	return ("keyk","kek")
################################################################################

def unzipFile(zipFile,password):
	print "unzipping zipfile: ",zipFile
	print "password: ",password
	success = os.system("unzip -n -P" + password + " " + zipFile)
	return success

#PORTS PART
#https://stackoverflow.com/questions/1541797/check-for-duplicates-in-a-flat-list
def arePortsUnique(ports):
	print len(ports)
	print len(set(ports))
	if len(ports) == len(set(ports)):
		return True
	return False

#https://stackoverflow.com/questions/2838244/get-open-tcp-port-in-python
def get_open_port():
	import socket
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	s.bind(("",0))
	s.listen(1)
	port = s.getsockname()[1]
	s.close()
	return port

# def communicateWithNSA(port,password):
# 	ncCommand = "nc -l 128.114.59.29 " + str(port) + " &"
# 	echoCommand = "echo " + password + " 128.114.59.29 " + str(port) + " | nc 128.114.59.29 2001" 
# 	subprocess.check_output(ncCommand,shell=True)
# 	time.sleep(.15)
# 	success = subprocess.check_output(echoCommand,shell=True)

def listenForResponse(port):
	#echoCommand = echoCommand = "echo " + password + " 128.114.59.29 " + str(port) + " | nc 128.114.59.42 2001" 
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	s.bind(("128.114.59.29",port))
	s.listen(5)
	while True:
		connection,address = s.accept()
		buff = connection.recv(256)
		if len(buff) > 0:
			print buff
			break
	s.close()
	return buff
def doNSACrack(port, password):
	#os.chdir(workingDir)
	#passwordFile = subprocess.check_output("ls *.passwd.pcap",shell=True)
	#password = readPCAP(passwordFile)
	print "encrypted password: ",password
	print "port ",port
	ncCommand = "nc -l 128.114.59.29 " + str(port) + " &"
	echoCommand = "echo " + password + " 128.114.59.29 " + str(port) + " | nc 128.114.59.42 2001" 
	time.sleep(.15)
	success = "BUSY"
	while(success != "OK"):
		success = subprocess.check_output(echoCommand,shell=True)
		success = success.rstrip("\n")
		if success != "OK":
			print "NSA busy"
			time.sleep(5)
	#output = subprocess.check_output(ncCommand, shell=True)
	output = listenForResponse(port)
	#print "output is ",output
	return output.split()[1].rstrip()

def getPcapOrder():
	regex = "[0-9]+"
	fileList = list()
	for file in os.listdir("."):
		quest = re.search(regex,file)
		if quest is None:
			print "None file found in ",os.listdir(".")
		else: 
			fileList.append(int(re.search(regex,file).group(0)))
	fileList.sort()
	#print fileList
	return fileList

def runWithNSA(path):
	os.chdir(path)
	newPath = os.getcwd()
	fileList = getPcapOrder()
	filePrefix = "mypcap"
	encryptedPasswordFile = filePrefix+str(fileList[0])
	zipFile = filePrefix+str(fileList[1])
	ivFile = filePrefix+str(fileList[2])
	cipherFile = filePrefix+str(fileList[3])
	port = get_open_port()
	encryptedPassword = readFromFile(encryptedPasswordFile).rstrip()
	print "doing nsa on ",path
	plainPassword = doNSACrack(port,encryptedPassword)
	print "plain password for " + path + " is ",plainPassword
	print "cwd is ",os.getcwd()
	writeToFile(newPath + "/password",plainPassword)

def runWithPasswords(decryptPath, path):
	os.chdir(path)
	newPath = os.getcwd()
	fileList = getPcapOrder()
	filePrefix = "mypcap"
	encryptedPasswordFile = filePrefix+str(fileList[0])
	zipFile = filePrefix+str(fileList[1])
	ivFile = filePrefix+str(fileList[2])
	writeToFile(ivFile,readFromFile(ivFile).rstrip()) #remove whitespace from end of iv.
	cipherFile = filePrefix+str(fileList[3])
	plainPassword = readFromFile("password").rstrip()
	unzip = unzipFile(zipFile,plainPassword)
	if unzip == 0:
		print "error unzipping file. ",zipFile
	key,message = findKeyAndMessage(decryptPath,cipherFile,"key",ivFile)
	writeToFile("message.plain",message)

#https://stackoverflow.com/questions/973473/getting-a-list-of-all-subdirectories-in-the-current-directory
def getDirs(path):
	directories = list()
	for item in os.listdir(path):
		if os.path.isdir(path + "/" + item):
			directories.append(item)
	return directories

def runThroughDirs(path):
	dirList = getDirs(path)
	dirList.sort()
	currentPath = os.getcwd()
	threads = list()
	for directory in dirList:
		#try:
		os.chdir(currentPath)
		t = Thread(target=runWithNSA,args=((path + "/" + directory),))
		t.start()
		threads.append(t)
		time.sleep(5)
	for t in threads:
		t.join()

		#except:
		#	print "Error starting thread for directory ",directory

#doNSACrack(get_open_port(),"toP057Uf1t742")
#content = readPCAP("key.zip.pcap")
#writeZipFile("key.zip",content)
#findKeyAndMessage(".","../message.cipher","../key","../iv.plain" )


#runThroughDirs("../Phase2/pcapfiles/mypcaps/")

decryptPath = os.getcwd()
os.chdir("./pcapfiles/mypcaps/")
workingDir = os.getcwd()
dirList = getDirs(os.getcwd())
for directory in dirList:
	runWithPasswords(decryptPath,directory)
	os.chdir(workingDir)
