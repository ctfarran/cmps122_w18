#!/usr/bin/env python

# Write all pcap files to allpcaps directory

import os, socket, datetime, time

IP = '128.114.59.42'
PORT = 5001

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((IP, PORT))
cwd = os.getcwd()

s.settimeout(36000)
counter = 0


while True:
	file_path = cwd + "/pcapfiles/finalpcaps/" + "mypcap" + str(counter) + ".pcap"
	pcapfile = open(file_path, "wb")
	
	try:
		pcapbuffer = s.recv(65536)
	except socket.timeout:
		print("Socket timeout")
		break
	
	for item in pcapbuffer:
		if item is not None:
			pcapfile.write(item)

	counter = counter + 1
	pcapfile.close()

s.close()

