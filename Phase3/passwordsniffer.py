#!/usr/bin/env python

import os, socket, datetime

IP = '128.114.59.42'
PORT = 5001

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((IP, PORT))
cwd = os.getcwd()

s.settimeout(150)
counter = 0

while True:
	now = datetime.datetime.now()
	hours = now.hour
	minutes = now.minute	

	if counter >= 2880 or (hours == 1 and minutes >= 45):
		print("Breaking from Loop")
		break

	file_path = cwd + "/pcapfiles/allpcaps/" + "mypcap" + str(counter) + ".pcap"
	pcapfile = open(file_path, "wb")

	# If we are not able to capture anymore pakcets for 2 min. 30 sec. break form the loop
	# Professor stops sending pakcets for a little bit
	try:
		pcapbuffer = s.recv(65536)
		counter = counter + 1
	except socket.timeout:
		continue

	for item in pcapbuffer:
		if item is not None:
			pcapfile.write(item)

	pcapfile.close()

s.close()	
