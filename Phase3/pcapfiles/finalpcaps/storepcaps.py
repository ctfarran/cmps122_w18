#!/usr/bin/env python

import os, glob
import shutil

cwd = os.getcwd()
goodpcaps_list = glob.glob("mypcap*")

# Sort every element pcaps by their end number
goodpcaps_list = sorted(goodpcaps_list, key=lambda x: int(x[6:]))

x = 0
dir_count = 0
while x != len(goodpcaps_list)-1:
	pcap_0 = goodpcaps_list[x]
	
	data = None	
	with open(pcap_0, "r") as f:
		data = f.read().rstrip()
	
	if data == "parg1/WTS.A8E":	
		x = x + 6
	elif data[:3] == "AAA" or data == "GET / HTTP/1.1":
		x = x + 1
	else:
		pcap_dir = cwd + "/dir" + str(dir_count)
		if not os.path.exists(pcap_dir):
			os.makedirs(pcap_dir)
			pcap_1 = goodpcaps_list[x+1]
			pcap_2 = goodpcaps_list[x+2]
			pcap_3 = goodpcaps_list[x+3]

			shutil.move(pcap_0, pcap_dir)
			shutil.move(pcap_1, pcap_dir)
			shutil.move(pcap_2, pcap_dir)
			shutil.move(pcap_3, pcap_dir)
		
		dir_count = dir_count + 1
		x = x + 4

extrapcaps_list = glob.glob("mypcap*")
for pcap in extrapcaps_list:
	os.remove(pcap)	
