#!/usr/bin/env python

import os, glob, subprocess, collections

patternfd = open("patterns.txt", "w+")
saltsfd = open("salts.txt", "w+")

passwordsfd = open("passwords.txt", "w+")

def insert_dict(line, d):
	print("Line: {}".format(line))
	
	i = 0
	while i != len(line) - 1:
		c = line[i]
		d[c] = line[i+1]	
	
		if i==len(line)-2:
			break
		i+=2
	
	return d

d = {}
sort_list = []

dir_list = glob.glob("./dir*")

dir_list = sorted(dir_list, key=lambda x: int(x[5:]))

for counter, mydir in enumerate(dir_list):
	os.chdir(mydir)
	fh = open("password")
	e_str = "mypcap" + str(counter)
	fs = open(e_str)

	line = fh.readline().rstrip()
	e_txt = fs.readline().rstrip()

	passwordsfd.write(line)
	passwordsfd.write("\n")

	saltsfd.write(e_txt[0:2])
	saltsfd.write("\n")

	d = insert_dict(line, d)
	fh.close()
	os.chdir("..")

od = collections.OrderedDict(sorted(d.items()))

for key, value in od.items():
	patternfd.write(key)
	patternfd.write(" ")
	patternfd.write(value)
	patternfd.write("\n")		

patternfd.close()
saltsfd.close()
