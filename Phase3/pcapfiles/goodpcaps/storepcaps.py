#!/usr/bin/env python

import os, glob
import shutil

cwd = os.getcwd()
goodpcaps_list = glob.glob("mypcap*")
goodpcaps_list = sorted(goodpcaps_list, key=lambda x: int(x[6:]))

for dir_count, x in enumerate(goodpcaps_list):
	pcap_dir = cwd+ "/dir" + str(dir_count)
	if not os.path.exists(pcap_dir):
		os.makedirs(pcap_dir)
		shutil.move(x, pcap_dir)
