#!/usr/bin/env python

import sys
from scapy.all import *
from scapy.utils import *

def getLoad(fileName):
	packets = rdpcap(fileName)
	packet = packets[0]
	return packet.load

def writeFile(load, fileName):
	f = open(fileName, "wb")
	f.write(load)
	f.close
fileName = sys.argv[1]
load = getLoad(fileName)
writeFile(load, fileName[:-5])

