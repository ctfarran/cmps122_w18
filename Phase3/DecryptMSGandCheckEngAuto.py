#!/usr/bin/env python

import os, sys
import M2Crypto
import operator

from nltk.corpus import wordnet

#sudo pip install nltk
# python
# >>> import nltk
# >>> nltk.download('words')
#use
# python DecryptMSGandCheckEngAuto.py message_cipher key_file iv_plain

message_cipher = sys.argv[1]
key_file = sys.argv[2]
iv_plain = sys.argv[3]

#M2Crypto.Rand.rand_seed (os.urandom (1024))

def checkEnglishRatio(message):
	from nltk.corpus import words
	#nltk.download('words')
	total = 0.0
	englishWordCount = 0.0
	for word in message.split():
		word = word.rstrip().rstrip(",.?):;\t '").lstrip("('\t ").lower()
		if word in words.words():
			englishWordCount +=1.0
		total += 1.0
	print "wordcount is ",englishWordCount
	print "total is ",total
	return float(englishWordCount / total)


def encrypt_file(key, in_filename, out_filename,iv):
    cipher=M2Crypto.EVP.Cipher('aes_128_cbc',key,iv, op=1)
    with open(in_filename, 'rb') as infile:
        with open(out_filename, 'wb') as outfile:
         # outfile.write(b)
          while True:
            buf = infile.read(1024)
            if not buf:
                break
            outfile.write(cipher.update(buf))

          outfile.write( cipher.final() )  
          outfile.close()
        infile.close()

def decrypt_file(key, in_filename, out_filename,iv):
    cipher = M2Crypto.EVP.Cipher("aes_128_cbc",key , iv, op = 0)
    with open(in_filename, 'rb') as infile: 
        with open(out_filename, 'wb') as outfile:
          while True:
            buf = infile.read(1024)
            if not buf:
                break
            try:
                outfile.write(cipher.update(buf))
            except:
                print ("here")
          outfile.write( cipher.final() )
          print (outfile.read())  
          outfile.close()
        infile.close()



for x in range(0, 992):
	fo = open(key_file,"r")
	fo.seek(x)
	top = 32 + x
	key = fo.read(top - x) 
	with open(iv_plain) as fl:
		iv = fl.read().replace('\n','')
	filename = "output" + str(x)	
	try:
		decrypt_file(key,message_cipher,filename,iv)
		with open(filename, 'r') as fin:
			print(fin.read())
		
	except IOError:
		continue
	except:
		os.remove(filename)

for x in range(0, 992):
	filename = "output" + str(x)
	try:
		with open (filename, "r") as fl:
			text = fl.read()
			#print(text)
			print (filename)
			x = checkEnglishRatio(text)
			#print(x)
			# Choose the precntage of english words you want here ex: 0.1
			if (x > 0.1):
				print("This could be the one:")
				print(text)
	except IOError:
                continue
