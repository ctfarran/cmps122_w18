#!/usr/bin/env python

import os, glob
import shutil

cwd = os.getcwd()
pcaps_list = glob.glob("mypcap*")

# Sort every element pcaps by their end number
pcaps_list = sorted(pcaps_list, key=lambda x: int(x[6:]))

x = 0
dir_count = 0
while x >= len(pcaps_list)-1:
	
	pcap_dir = cwd + "/dir" + str(dir_count)
	if not os.path.exists(pcap_dir):
		os.makedirs(pcap_dir)
		pcap_0 = pcaps_list[x]
		pcap_1 = pcaps_list[x+1]
		pcap_2 = pcaps_list[x+2]
		pcap_3 = pcaps_list[x+3]
		pcap_4 = pcaps_list[x+4]
		pcap_5 = pcaps_list[x+5]

		shutil.move(pcap_0, pcap_dir)
		shutil.move(pcap_1, pcap_dir)
		shutil.move(pcap_2, pcap_dir)
		shutil.move(pcap_3, pcap_dir)
		shutil.move(pcap_4, pcap_dir)			
		shutil.move(pcap_5, pcap_dir)

	dir_count = dir_count + 1
	x = x + 6

"""
extrapcaps_list = glob.glob("mypcap*")
for pcap in extrapcaps_list:
	os.remove(pcap)	
"""
