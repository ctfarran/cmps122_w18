#!/usr/bin/env python

import os, glob
import random

dir_files = glob.glob("dir*")

for mydir in dir_files:
	os.chdir(mydir)
	pcap_files = glob.glob("mypcap*")
	pcap_files = sorted(pcap_files, key=lambda x: int(x[6:]))
	rand_int = random.randint(3, 4)
	os.remove(pcap_files[rand_int])
	os.chdir("..")
