#!/usr/bin/env python

import os, sys, subprocess

message_cipher = sys.argv[1]
iv_plain = sys.argv[2]
key = sys.argv[3]


cipher_file = open(message_cipher, "r")

cwd = os.getcwd()
CIPHER_PATH = message_cipher


key_file = open(key, "r")

with open(iv_plain) as f:
	iv_content = f.readlines()

IV = iv_content[0].rstrip()

key_counter = 0
while True:
	KEY = key_file.read(32).rstrip()
	if not KEY:
		break
	elif len(KEY) < 32:
		break
	else:
		PLAIN_PATH = message_cipher[:-9] +".plain" + str(key_counter)

		subprocess.call(["./decrypt", CIPHER_PATH, KEY, IV, PLAIN_PATH])

		statinfo = os.stat(PLAIN_PATH)
		if statinfo.st_size > 0:	
			print("Key: {}".format(KEY))
		else:
			os.remove(PLAIN_PATH)

		key_file.seek(key_counter+1)
		key_counter = key_counter + 1

cipher_file.close()
key_file.close()
