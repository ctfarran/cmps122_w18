#!/usr/bin/env python
import sys, datetime, time


def polybius_decrypt(cipherStr, poly_square, tok_index):
# infile = "message43.cipher.output438"
	tokens = list(cipherStr)
	#print tokens
	tokens_enum = iter(range(len(tokens)))
	tokens2 = []
	for i in tokens_enum:
		if tokens[i].isdigit() and tokens[i+1].isdigit():
			tokens2.append(tokens[i] +tokens[i+1])
			tokens_enum.next()
		elif(tokens[i] == ' ' and tokens[i+1] == ' '):
			tokens2.append(tokens[i])
			tokens_enum.next()
		else:
			tokens2.append(tokens[i])
	
	
	"""
	poly_square = \
	[['a','b','c','d','e'], \
	['f','g','h','i','j'], \
	['k','l','m','n','o'], \
	['p','q','r','s','t'], \
	['u','v','w','x','y']]
	"""


	for i, token in enumerate(tokens2):
		if(len(token) == 2):
			if tok_index == 0:
				tokens2[i] = poly_square[int(token[tok_index]) -1] [int(token[tok_index+1]) -1] #Flip tokens here
			elif tok_index == 1:
				tokens2[i] = poly_square[int(token[tok_index]) -1] [int(token[tok_index-1]) -1] #Flip tokens here
	message_plain = ''.join(tokens2)
	return message_plain

def try_shift_squares(cipher_txt, cruz_id_list):
	
	"""
	data = None
	with open("message43.cipher.output438", "r") as myfile:
		data=myfile.read()
	data = data.strip()
	"""
		
	poly_square = [[0 for i in range(5)] for j in range(5)]

	now = None
	microsecond = None
	ascii_count = 97
	for k in range(25):
		other_ascii = ascii_count
		for i in range(5):
			for j in range(5):
				if other_ascii > ord('y'):
					other_ascii -= 25

				poly_square[i][j] = chr(other_ascii)
				other_ascii+=1

		message1 = polybius_decrypt(cipher_txt, poly_square, 0)
		
		tokens = message1.split('\n')
		tokens = tokens[-1] 

		print(tokens)

		for cruz_id in cruz_id_list:
			if tokens == cruz_id.strip():
				return message1	


		message2 = polybius_decrypt(cipher_txt, poly_square, 1)
		
		tokens = message1.split('\n')
        tokens = tokens[-1]

        for cruz_id in cruz_id_list:
            if tokens == cruz_id.strip():
                return message1

		
		ascii_count+=1

	return "End"

def reverse_alphabet(cipher_txt, cruz_id_list):
	"""
	data = None
	with open("message43.cipher.output438", "r") as myfile:
		data=myfile.read()

	data = data.strip()
	"""

	poly_square = [[0 for i in range(5)] for j in range(5)]

	now = None
	microsecond = None
	ascii_count = 122
	for k in range(25):
		other_ascii = ascii_count
		for i in range(5):
			for j in range(5):
				if other_ascii < ord('a'):
					other_ascii += 25

				poly_square[i][j] = chr(other_ascii)
				other_ascii-=1

		message1 = polybius_decrypt(cipher_txt, poly_square, 0)
		
		tokens = message1.split('\n')
        tokens = tokens[-1]

        for cruz_id in cruz_id_list:
            if tokens == cruz_id.strip():
                return message1

		message2 = polybius_decrypt(cipher_txt, poly_square, 1)
		
		tokens = message1.split('\n')
        tokens = tokens[-1]

        for cruz_id in cruz_id_list:
            if tokens == cruz_id.strip():
                return message1

		ascii_count-=1

def decrypt_wrapper(cipher_txt, cruz_id_list):
	message1 = try_shift_squares(cipher_txt, cruz_id_list)
	if(message1 != "End"):
		print(message1)
		return message1

	message2 = reverse_alphabet(cipher_txt, cruz_id_list)
	if(message2 != "End"):
		print(message2)
		return message2

	return "Nothing"

start_time = time.time()


cipher_txt = None
with open("message43.cipher.output438", "r") as myfile:
	cipher_txt = myfile.read()
	cipher_txt = cipher_txt.strip()

cruz_id_list = open("cruzid").readlines()

decrypt_wrapper(cipher_txt, cruz_id_list)
#print("My program took", time.time() - start_time, "to run")
#print( polybius_decrypt("message43.cipher.output438", "message43.plain.output438"))
