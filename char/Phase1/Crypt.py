import os
import M2Crypto

#M2Crypto.Rand.rand_seed (os.urandom (1024))

def encrypt_file(key, in_filename, out_filename,iv):
    cipher=M2Crypto.EVP.Cipher('aes_128_cbc',key,iv, op=1)
    with open(in_filename, 'rb') as infile:
        with open(out_filename, 'wb') as outfile:
         # outfile.write(b)
          while True:
            buf = infile.read(1024)
            if not buf:
                break
            outfile.write(cipher.update(buf))

          outfile.write( cipher.final() )  
          outfile.close()
        infile.close()

def decrypt_file(key, in_filename, out_filename,iv):
    cipher = M2Crypto.EVP.Cipher("aes_128_cbc",key , iv, op = 0)
    with open(in_filename, 'rb') as infile: 
        with open(out_filename, 'wb') as outfile:
          while True:
            buf = infile.read(1024)
            if not buf:
                break
            try:
                outfile.write(cipher.update(buf))
            except:
                print ("here")
          outfile.write( cipher.final() )
          print (outfile.read())  
          outfile.close()
        infile.close()



for x in range(0, 992):
	fo = open("key","r")
	fo.seek(x)
	top = 32 + x
	key = fo.read(top - x) 
	#key = "503490CF2A24DBFABCD357B293FCCE87"
	with open("iv") as fl:
		#iv = "E7C6357864F322AF2EC892CC2D4E4EBA"
		iv = fl.read().replace('\n','')
		print(iv)
	filename = "output" + str(x)	
	try:
		decrypt_file(key,"message3.cipher",filename,iv)
		with open(filename, 'r') as fin:
			print fin.read()
	except:
		print "error with decrypt