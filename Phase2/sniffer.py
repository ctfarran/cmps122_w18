#!/usr/bin/env python

import os, socket, datetime, time

start = time.time()

IP = '128.114.59.42'
PORT = 5001

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((IP, PORT))
cwd = os.getcwd()
goodpcaps_dir = cwd + "/pcapfiles/goodpcaps/"

current_time_tuple = datetime.datetime.now()
current_time_str = str(current_time_tuple)
current_time_list = current_time_str.split(" ")
current_time = current_time_list[1]

begin_time = "01:58:00"
end_time = "02:30:00"

mid_time = "13:58:00"
end_mid_time = "14:30:00"

s.settimeout(150)
counter = 0
while True:
	current_time_tuple = datetime.datetime.now()
	current_time_str = str(current_time_tuple)
	current_time_list = current_time_str.split(" ")
	current_time = current_time_list[1]

	if current_time >= begin_time:
		goodpcaps_list = os.listdir(goodpcaps_dir)
		number_files = len(goodpcaps_list)			

	
		if current_time >= end_time or counter >= 3000:
			end = time.time()
			print("Time elapsed: {}".format(end - start))	
			break

		pcapstr = "mypcap" + str(counter) + ".pcap"	
		file_path = cwd + "/pcapfiles/allpcaps/" + "mypcap" + str(counter) + ".pcap"
		pcapfile = open(file_path, "wb")
		try:
			pcapbuffer = s.recv(65536)
		except:
			break
			
		for item in pcapbuffer:
			if item is not None:
				pcapfile.write(item)

		counter = counter + 1
		pcapfile.close()
s.close()

