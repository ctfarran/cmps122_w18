#!/usr/bin/env python

import os, glob, subprocess, collections

def insert_dict(line, d):
	print("Line: {}".format(line))
	
	i = 0
	while i != len(line) - 1:
		c = line[i]
		d[c] = line[i+1]	
	
		if i==len(line)-2:
			break

		i+=2
	
	return d

d = {}
sort_list = []

dir_list = glob.glob("./dir*")

dir_list = sorted(dir_list, key=lambda x: int(x[5:]))


for mydir in dir_list:
	os.chdir(mydir)
	fh = open("password")
	line = fh.readline().rstrip()

	d = insert_dict(line, d)
	# print("{} , {}".format(line, count))	
	# sort_list.append(line)
	fh.close()
	os.chdir("..")

od = collections.OrderedDict(sorted(d.items()))
print(od)

"""
sort_list = sorted(sort_list)

for count, e in enumerate(sort_list):
	print("{} {}".format(e, count))
"""
