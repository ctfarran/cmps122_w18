import os,subprocess

def readPasswords():
	passwordList = list()
	passwords = subprocess.check_output("cat *5*/password",shell=True)
	dex = 0
	for i in range(len(passwords)/6):
		passwordList.append(passwords[dex:dex+6])
		dex += 6
	return passwordList

def printOrds(passwords):
	for password in passwords:
		ordList = list()
		for c in password:
			ordList.append(ord(c))
		print "password:",password
		print "ordList: ",ordList
		print "sum: ",sum(ordList)

passwords = readPasswords()
printOrds(passwords)
