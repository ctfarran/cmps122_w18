import re
import EnglishAndBinaryHelpers

def readFromFile(fileName):
	f = open(fileName,"r")
	output = ""
	for line in f:
		output += line
	f.close()
	return output
def writeToFile(fileName,content):
	f = open(fileName,"w")
	f.write(content)
	f.close()
def decodeCaesar(cipher,index):
	asciiNumRange = range(48,58)
	asciiUpperRange = range(65,91)
	asciiLowerRange = range(97,123)
	message = ""
	for letter in cipher:
		o = ord(letter)
		if (o not in asciiUpperRange and o not in asciiLowerRange) or o in asciiNumRange:
			yield o
		else: 
			newO = o + index % 26
			if o in asciiLowerRange and newO in asciiLowerRange:
				yield newO 
			elif o in asciiUpperRange and newO in asciiUpperRange:
				yield newO 
			else:
				yield newO - 26

#https://stackoverflow.com/questions/8886947/caesar-cipher-function-in-python
def hailCaesar(cipher):
	for index in range(12,26):
		message =  (''.join(map(chr,decodeCaesar(cipher,index))))
		#print message
		if EnglishAndBinaryHelpers.checkEnglishRatio(message) > .6:
			print "index is ",index
			print message
			break
cipher = readFromFile("../Phase3/mag1")
hailCaesar(cipher)
