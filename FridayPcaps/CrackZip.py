#!/usr/bin/env python

import os, glob
import crypt, pwd

Chars = ['0s','1B','2w','3B','4h','5a','6t','7L','8p','9k','At','BB','Ch','Da','Ea','FC','Gs','Hf','Im','Ja','Km','Ls','Ma','Ni','Om','Pb','Qh','RW','St','Tf','Ui','Va','Wt','Xc','Yw','Zb','a0','b3','c6','d9','ec','gc','hC','ip','je','kd','lt','mi','nw','oS','pb','qT','rh','sC','tI','us','va','wA','xC','yH', 'zo']


os.chdir("dir56")
pcap_list = glob.glob("mypcap*")
pcap_list = sorted(pcap_list, key=lambda x: int(x[6:]))
with open(pcap_list[0], "r") as fl:
	hashPass = fl.readline().rstrip()
	for w1 in Chars:
		for w2 in Chars:
			for w3 in Chars:
				password = w1+w2+w3
				salt = hashPass[0:2]
				x = crypt.crypt(password, salt)
				if (x == hashPass):
					os.system("unzip -P" + " " + password + " " + pcap_list[1])
os.chdir("..")


"""
dir_list = glob.glob("dir*")

# Sort every element pcaps by their end number
dir_list = sorted(dir_list, key=lambda x: int(x[3:]))

for mydir in dir_list:
	os.chdir(mydir)
	pcap_list = glob.glob("mypcap*")
	pcap_list = sorted(pcap_list, key=lambda x: int(x[6:]))
	with open(pcap_list[0], "r") as fl:
		hashPass = fl.readline().rstrip()
		for w1 in Chars:
			for w2 in Chars:
				for w3 in Chars:
					password = w1+w2+w3
					salt = hashPass[0:2]
					x = crypt.crypt(password, salt)
					if (x == hashPass):
						os.system("unzip -P" + " " + password + " " + pcap_list[1])	
	os.chdir("..")
"""
