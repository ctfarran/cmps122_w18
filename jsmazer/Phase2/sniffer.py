#!/usr/bin/env python

# Write all pcap files to allpcaps directory

import os, socket, datetime

IP = '128.114.59.42'
PORT = 5001

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((IP, PORT))
cwd = os.getcwd()

s.settimeout(150)
counter = 0

while True:
	now = datetime.datetime.now()
	hours = now.hour
	minutes = now.minute
	
	# if time >= 13:58:00	
	if (hours == 1 and minutes >= 58): 

		# if time >= 14:30:00 or counter >= 3000		
		if (hours >= 2 and minutes >= 30)  or counter >= 3000:
			break

		pcapstr = "mypcap" + str(counter) + ".pcap"	
		file_path = cwd + "/backup_pcapfiles/allpcaps/" + "mypcap" + str(counter) + ".pcap"
		pcapfile = open(file_path, "wb")
	
		# If we are not able to capture anymore pakcets for 2 min. 30 sec. break form the loop
		# Professor stops sending pakcets for a little bit	
		try:
			pcapbuffer = s.recv(65536)
		except socket.timeout:
			break
			
		for item in pcapbuffer:
			if item is not None:
				pcapfile.write(item)

		counter = counter + 1
		pcapfile.close()
s.close()

