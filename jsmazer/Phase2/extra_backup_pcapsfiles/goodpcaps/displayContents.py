#!/usr/bin/env python

import os, glob

cwd = os.getcwd()
goodpcaps_list = glob.glob("mypcap*") 

displayfd = open("displayPCaps.txt", "w+")

# Sort every element in the list in order
goodpcaps_list = sorted(goodpcaps_list, key=lambda x: int(x[6:]))

# write the contents of the pcaps into displayPCaps.txt
count = 1
for pcap in goodpcaps_list:
	with open(pcap, "r") as f:
		displayfd.write(f.read())

	if count % 4 == 0:
		displayfd.write("\n\n")

	count = count + 1
	
displayfd.close()

