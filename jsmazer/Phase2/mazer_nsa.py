#!/usr/bin/env python

import os, socket, threading, subprocess

#https://stackoverflow.com/questions/2838244/get-open-tcp-port-in-python
def get_open_port():
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	s.bind(("",0))
	s.listen(1)
	port = s.getsockname()[1]
	s.close()
	return port

def listen_on_port(port):
	print("Listening on port: {}".format(port))
	subprocess.call(["nc", "-l", "128.114.59.29", str(port)])

def listen_with_ports():
	port = get_open_port()
	listen_on_port(port)

thread_list = []
x = 0
#while x != 66:
t = threading.Thread(target = listen_with_ports, args = (  ))
t.start()
thread_list.append(t)

for t in thread_list:
	t.join()
