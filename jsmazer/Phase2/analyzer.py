#!/usr/bin/env python

import os, glob
from scapy.all import *

def readPCap(fileName):
	try:	
		pack = rdpcap(fileName)		
		packet = pack[0]

		if packet.getlayer(Raw) is not None:
			return packet.getlayer(Raw).load
		else:
			return None
	except:
		print("Filename: {}".format(fileName))
		return None

def writeContent(fileName, content):
	pcapfd = open(fileName, "w")
	pcapfd.write(content)
	pcapfd.close()


cwd = os.getcwd()
allpcaps_path = cwd + "/pcapfiles/allpcaps/"
list_of_pcaps = os.listdir(allpcaps_path)


for pcap in list_of_pcaps:
	pcap_path = cwd + "/pcapfiles/allpcaps/" + pcap
	content = readPCap(pcap_path)
		
	if content is not None:
		# goodpcaps_path = cwd + "/pcapfiles/goodpcaps" + pcap[:-5]
		# writeContent(goodpcaps_path, content)
		mypcaps_path = cwd + "/pcapfiles/mypcaps/" + pcap[:-5]
		writeContent(mypcaps_path, content)	
