#include <stdio.h>
#include <stdlib.h>
#include <openssl/conf.h>
#include <openssl/evp.h>
#include <openssl/err.h>
#include <string.h>
#include <setjmp.h>

#include "crypto.h"

int main(int argc, char*argv[])
{
	unsigned char *key = (unsigned char *) "6E2B2B8CBCB471AA4B2DD3641CAF044F";
	unsigned char *iv = (unsigned char *) "B224075FE6EAA70A9A0CD324D857AF82";


	FILE *myfile = fopen("message.plain", "w+");

	FILE *cryptfile = fopen("jsmazer.message.cipher", "r");  // Open the file in binary mode
	fseek(cryptfile, 0L, SEEK_END);          // Jump to the end of the file
	int filelen = ftell(cryptfile);             // Get the current byte offset in the file
	rewind(cryptfile);  	                   // Jump back to the beginning of the file


	unsigned char decrypt_text[filelen+1];
	decrypt_text[filelen] = '\0';
	
	unsigned char cipher_text[filelen+1];
	cipher_text[filelen] = '\0';


	printf("Filelen: %d\n", filelen);

	fread(cipher_text, 1, filelen, cryptfile);	

	int decryptlen = decrypt(cipher_text, filelen-1, key, iv, decrypt_text);
	
	printf("Decryptlen: %d\n", decryptlen);


	decrypt_text[decryptlen] = '\0';


	fwrite(decrypt_text, sizeof(char), decryptlen, myfile);	
			
	fclose(cryptfile);
	fclose(myfile);
}
