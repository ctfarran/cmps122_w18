#!/usr/bin/env python

import os, sys
from scapy.all import *
from scapy.utils import *

# Caleb Farrand ctfarran
# Avery Iosa
# Julius Mazer jmazer

#Takes in a .zip.pcap file and returns a .zip file.
# Courtesy of https://stackoverflow.com/questions/34293209/pythonscapy-raw-load-found-how-to-access
def getZipFile(fileName):
	pack = rdpcap(fileName)
	print pack
	packet = pack[0]
	return packet.getlayer(Raw).load
def writeZipFile(zipFileName,content):
	zipFile = open(zipFileName,"w")
	zipFile.write(content)
	zipFile.close()
	#TODO write code to verify zip file was written correctly

content = getZipFile(sys.argv[1])
myfile = sys.argv[1]
myfile = myfile[:-5] + "js"
writeZipFile(myfile, content)
