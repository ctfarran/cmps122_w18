#include <stdio.h>
#include <stdlib.h>
#include <openssl/conf.h>
#include <openssl/evp.h>
#include <openssl/err.h>
#include <string.h>
#include <setjmp.h>

#include "crypto.h"

int main(int argc, char*argv[])
{
	unsigned char *key = (unsigned char *) "503490CF2A24DBFABCD357B293FCCE87";
	unsigned char *iv = (unsigned char *) "E7C6357864F322AF2EC892CC2D4E4EBA";


	FILE *myfile = fopen("myplain.txt", "w+");

	FILE *plainfile = fopen("message.plain", "r");

	
	FILE *cryptfile = fopen("message.cipher", "r");  // Open the file in binary mode
	fseek(cryptfile, 0L, SEEK_END);          // Jump to the end of the file
	int filelen = ftell(cryptfile);             // Get the current byte offset in the file
	rewind(cryptfile);  	                   // Jump back to the beginning of the file


	unsigned char decrypt_text[3000];
	memset(decrypt_text, '\0', 3000);
	
	unsigned char cipher_text[3000];
	memset(cipher_text, '\0', 3000);

	unsigned char plain_text[3000];
    memset(plain_text, '\0', 3000);


	fread(cipher_text, 1, filelen, cryptfile);	

	int decryptlen = decrypt(cipher_text, filelen-1, key, iv, decrypt_text);
	
	printf("Filelen: %d\n", filelen);
	printf("Decryptlen: %d\n", decryptlen);


	decrypt_text[decryptlen] = '\0';


	fwrite(decrypt_text, sizeof(char), decryptlen, myfile);	
			
	/*	//Testing encryption
	char c;
	int i = 0;
	while(1)
	{
		c = fgetc(plainfile);
		if(feof(plainfile))
			break;
		plain_text[i] = c;
		i++;	
	}

	printf("File len: %d\n", filelen);	

	int cipherlen = encrypt(plain_text, strlen((char *)plain_text), key, iv, cipher_text);

	printf("Cipher len: %d\n", cipherlen);

	int decryptlen = decrypt(cipher_text, cipherlen, key, iv, decrypt_text);

	decrypt_text[decryptlen] = '\0';

	printf("Decrypt len: %d\n", decryptlen);
	printf("Decrpy text: \n");
	printf("%s", decrypt_text);
	*/

	fclose(cryptfile);
	fclose(myfile);
	fclose(plainfile);
}
