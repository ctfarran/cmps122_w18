#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <openssl/conf.h>
#include <openssl/evp.h>
#include <openssl/err.h>

#include "crypto.h"

int main(int argc, char *argv[])
{
	char *CIPHER_FILE = argv[1];
	unsigned char *KEY = (unsigned char *) argv[2];
	unsigned char *IV = (unsigned char *) argv[3];
	char *PLAIN_FILE = argv[4];

	FILE *crypt_file = fopen(CIPHER_FILE, "rb");
	fseek(crypt_file, 0, SEEK_END);
	int filelen = ftell(crypt_file);
	rewind(crypt_file);

	FILE *plain_file = fopen(PLAIN_FILE, "w"); 

	unsigned char decrpyt_text[filelen+1];
	decrpyt_text[filelen] = '\0';

	unsigned char cipher_text[filelen+1];
	cipher_text[filelen] = '\0';


	fread(cipher_text, filelen, 1, crypt_file);

	
	//printf("%s\n", KEY);
	//printf("%s\n", IV);

	decrypt(cipher_text, filelen-1, KEY, IV, decrpyt_text);

	char c;
    for(int i=0;i<strlen(decrpyt_text);i++)
    {
        c = decrpyt_text[i];
        if(c != '\r')
        {
            fputc(c, plain_file);
        }
    }

	fclose(crypt_file);	
	fclose(plain_file);
}
